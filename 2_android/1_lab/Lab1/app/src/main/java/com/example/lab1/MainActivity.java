package com.example.lab1;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onGetSurnameButtonClick(View view) {
        TextView textField=(TextView) findViewById(R.id.text_field);
        textField.setText(R.string.surname);
    }
}
