package com.example.lab21;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onGetStudentsButtonClick(View view){
        Spinner gradesSpinner=(Spinner)findViewById(R.id.grades);
        TextView resultTextField=(TextView)findViewById(R.id.result);
        String grade=(String)gradesSpinner.getSelectedItem();
       final StringBuilder result=new StringBuilder();
        List<String> listOfStudents=Repository.getStudentsWithGrade(grade);
        if(listOfStudents.isEmpty())
            resultTextField.setText("no such students");
        else {
            for (int i=0;i<listOfStudents.size();i++) {
                result.append(i+1+". ");
                result.append(listOfStudents.get(i));
                result.append("\n");
            }
            resultTextField.setText(result);
        }
    }
}
